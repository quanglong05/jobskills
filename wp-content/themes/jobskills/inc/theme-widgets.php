<?php

/**
 * Recent_Posts widget class
 *
 * @since 2.8.0
 */
class Jobskills_Widget_Recent_Posts extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_recent_entries', 'description' => __("Your site&#8217;s most recent Posts."));
        parent::__construct('recent-posts', __('Recent Posts Jobskills'), $widget_ops);
        $this->alt_option_name = 'widget_recent_entries';

        add_action('save_post', array($this, 'flush_widget_cache'));
        add_action('deleted_post', array($this, 'flush_widget_cache'));
        add_action('switch_theme', array($this, 'flush_widget_cache'));
    }

    function widget($args, $instance)
    {
        $cache = array();
        if (!$this->is_preview()) {
            $cache = wp_cache_get('widget_recent_posts', 'widget');
        }

        if (!is_array($cache)) {
            $cache = array();
        }

        if (!isset($args['widget_id'])) {
            $args['widget_id'] = $this->id;
        }

        if (isset($cache[$args['widget_id']])) {
            echo $cache[$args['widget_id']];
            return;
        }

        ob_start();
        extract($args);

        $title = (!empty($instance['title']) ) ? $instance['title'] : __('Recent Posts');

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', $title, $instance, $this->id_base);

        $number = (!empty($instance['number']) ) ? absint($instance['number']) : 5;
        if (!$number)
            $number = 5;
        $show_date = isset($instance['show_date']) ? $instance['show_date'] : false;

        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query(apply_filters('widget_posts_args', array(
                    'posts_per_page' => $number,
                    'no_found_rows' => true,
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => true
        )));

        if ($r->have_posts()) :
            ?>
            <div class="widget">
                <h4><?php if ($title) echo $title ?></h4>
                <ul class="recent_post">
                    <?php while ($r->have_posts()) : $r->the_post(); ?>
                        <li>
                            <?php if ($show_date) : ?>
                                <i class="icon-calendar-empty"></i> <?php echo get_the_date('S M, Y'); ?>
                            <?php endif; ?>
                            <div><a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a></div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;

        if (!$this->is_preview()) {
            $cache[$args['widget_id']] = ob_get_flush();
            wp_cache_set('widget_recent_posts', $cache, 'widget');
        } else {
            ob_end_flush();
        }
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        $instance['show_date'] = isset($new_instance['show_date']) ? (bool) $new_instance['show_date'] : false;
        $this->flush_widget_cache();

        $alloptions = wp_cache_get('alloptions', 'options');
        if (isset($alloptions['widget_recent_entries']))
            delete_option('widget_recent_entries');

        return $instance;
    }

    function flush_widget_cache()
    {
        wp_cache_delete('widget_recent_posts', 'widget');
    }

    function form($instance)
    {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $number = isset($instance['number']) ? absint($instance['number']) : 5;
        $show_date = isset($instance['show_date']) ? (bool) $instance['show_date'] : false;
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts to show:'); ?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

        <p><input class="checkbox" type="checkbox" <?php checked($show_date); ?> id="<?php echo $this->get_field_id('show_date'); ?>" name="<?php echo $this->get_field_name('show_date'); ?>" />
            <label for="<?php echo $this->get_field_id('show_date'); ?>"><?php _e('Display post date?'); ?></label></p>
        <?php
    }

}

/**
 * Search widget class
 *
 * @since 2.8.0
 */
class Jobskills_Widget_Search extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_search', 'description' => __("A search form for your site."));
        parent::__construct('search', _x('Search Jobskills', 'Search widget'), $widget_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        echo '<div class="widget" style="margin-top:15px;">';
        if ($title)
            echo $before_title . $title . $after_title;

        // Use current theme search form if it exists
        get_search_form();

        echo '</div>';
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => ''));
        $title = $instance['title'];
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
        <?php
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $new_instance = wp_parse_args((array) $new_instance, array('title' => ''));
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

}

/**
 * Tag cloud widget class
 *
 * @since 2.8.0
 */
class Jobskills_Tag_Cloud extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('description' => __("A cloud of your most used tags."));
        parent::__construct('tag_cloud', __('Tag Cloud Jobskills'), $widget_ops);
    }

    function widget($args, $instance)
    {
        extract($args);
        $current_taxonomy = $this->_get_current_taxonomy($instance);
        if (!empty($instance['title'])) {
            $title = $instance['title'];
        } else {
            if ('post_tag' == $current_taxonomy) {
                $title = __('Tags');
            } else {
                $tax = get_taxonomy($current_taxonomy);
                $title = $tax->labels->name;
            }
        }

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', $title, $instance, $this->id_base);

        echo '<div class="widget tags add_bottom_30">';
        if ($title)
            echo '<h4>' . $title . '</h4>';
        /**
         * Filter the taxonomy used in the Tag Cloud widget.
         *
         * @since 2.8.0
         * @since 3.0.0 Added taxonomy drop-down.
         *
         * @see wp_tag_cloud()
         *
         * @param array $current_taxonomy The taxonomy to use in the tag cloud. Default 'tags'.
         */
        wp_tag_cloud(apply_filters('widget_tag_cloud_args', array(
            'taxonomy' => $current_taxonomy,
            'unit' => 'px',
            'smallest' => 13,
        )));

        echo "</div>\n";
    }

    function update($new_instance, $old_instance)
    {
        $instance['title'] = strip_tags(stripslashes($new_instance['title']));
        $instance['taxonomy'] = stripslashes($new_instance['taxonomy']);
        return $instance;
    }

    function form($instance)
    {
        $current_taxonomy = $this->_get_current_taxonomy($instance);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:') ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php
            if (isset($instance['title'])) {
                echo esc_attr($instance['title']);
            }
            ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Taxonomy:') ?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('taxonomy'); ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>">
                <?php
                foreach (get_taxonomies() as $taxonomy) :
                    $tax = get_taxonomy($taxonomy);
                    if (!$tax->show_tagcloud || empty($tax->labels->name))
                        continue;
                    ?>
                    <option value="<?php echo esc_attr($taxonomy) ?>" <?php selected($taxonomy, $current_taxonomy) ?>><?php echo $tax->labels->name; ?></option>
                <?php endforeach; ?>
            </select></p><?php
    }

    function _get_current_taxonomy($instance)
    {
        if (!empty($instance['taxonomy']) && taxonomy_exists($instance['taxonomy']))
            return $instance['taxonomy'];

        return 'post_tag';
    }

}

/**
 * Text widget class
 *
 * @since 2.8.0
 */
class Jobskills_Widget_Text extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML.'));
        $control_ops = array('width' => 400, 'height' => 350);
        parent::__construct('text', __('Text Jobskills'), $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        /**
         * Filter the content of the Text widget.
         *
         * @since 2.3.0
         *
         * @param string    $widget_text The widget content.
         * @param WP_Widget $instance    WP_Widget instance.
         */
        $text = apply_filters('widget_text', empty($instance['text']) ? '' : $instance['text'], $instance);
        echo '<div class="widget">';
        if (!empty($title)) {
            echo '<h4>' . $title . '</h4>';
        }
        ?>
        <p><?php echo!empty($instance['filter']) ? wpautop($text) : $text; ?></p>
        <?php
        echo "</div>\n";
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        if (current_user_can('unfiltered_html'))
            $instance['text'] = $new_instance['text'];
        else
            $instance['text'] = stripslashes(wp_filter_post_kses(addslashes($new_instance['text']))); // wp_filter_post_kses() expects slashed
        $instance['filter'] = isset($new_instance['filter']);
        return $instance;
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => '', 'text' => ''));
        $title = strip_tags($instance['title']);
        $text = esc_textarea($instance['text']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

        <p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label></p>
        <?php
    }

}

/**
 * Text widget class
 *
 * @since 2.8.0
 */
class Jobskills_Widget_Address extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML.'));
        parent::__construct('address', __('Address Jobskills'), $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        /**
         * Filter the content of the Text widget.
         *
         * @since 2.3.0
         *
         * @param string    $widget_text The widget content.
         * @param WP_Widget $instance    WP_Widget instance.
         */
        $home = apply_filters('widget_home', empty($instance['home']) ? '' : $instance['home'], $instance);
        $phone = apply_filters('widget_phone', empty($instance['phone']) ? '' : $instance['phone'], $instance);
        $email = apply_filters('widget_email', empty($instance['email']) ? '' : $instance['email'], $instance);
        if (!empty($title)) {
            echo $before_title . $title . $after_title;
        }
        ?>
        <ul id="contact-info">
            <li><i class="icon-home"></i>  <?php echo $home; ?></li>
            <li><i class="icon-phone"></i>  <?php echo $phone; ?></li>
            <li><i class="icon-email"></i>  <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
        </ul>
        <hr>
        <?php
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['home'] = strip_tags($new_instance['home']);
        $instance['phone'] = strip_tags($new_instance['phone']);
        $instance['email'] = strip_tags($new_instance['email']);
        return $instance;
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => ''));
        $title = strip_tags($instance['title']);
        $home = esc_textarea($instance['home']);
        $phone = esc_textarea($instance['phone']);
        $email = esc_textarea($instance['email']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Home:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('home'); ?>" name="<?php echo $this->get_field_name('home'); ?>" type="text" value="<?php echo esc_attr($home); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" /></p>
        <?php
    }

}

/**
 * Text widget class
 *
 * @since 2.8.0
 */
class Jobskills_Widget_FollowUs extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML.'));
        $control_ops = array('width' => 400, 'height' => 350);
        parent::__construct('followus', __('Follow us Jobskills'), $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        /**
         * Filter the content of the Text widget.
         *
         * @since 2.3.0
         *
         * @param string    $widget_text The widget content.
         * @param WP_Widget $instance    WP_Widget instance.
         */
        $text = apply_filters('widget_text', empty($instance['text']) ? '' : $instance['text'], $instance);

        $link_fb = apply_filters('widget_link_fb', empty($instance['link_fb']) ? '' : $instance['link_fb'], $instance);
        $fb = apply_filters('widget_fb', empty($instance['fb']) ? '' : $instance['fb'], $instance);
        $link_twitter = apply_filters('widget_link_twitter', empty($instance['link_twitter']) ? '' : $instance['link_twitter'], $instance);
        $twitter = apply_filters('widget_twitter', empty($instance['twitter']) ? '' : $instance['twitter'], $instance);
        $link_google = apply_filters('widget_link_google', empty($instance['link_google']) ? '' : $instance['link_google'], $instance);
        $google = apply_filters('widget_google', empty($instance['google']) ? '' : $instance['google'], $instance);
        if (!empty($title)) {
            echo $before_title . $title . $after_title;
        }
        ?>
        <p><?php echo $text; ?></p>
        <ul id="follow_us_contacts">
            <li><a href="<?php echo $link_fb; ?>"><i class="icon-facebook"></i><?php echo $fb; ?></a></li>
            <li><a href="<?php echo $link_twitter; ?>"><i class="icon-twitter"></i><?php echo $twitter; ?></a></li>
            <li><a href="<?php echo $link_google; ?>"><i class="icon-google"></i><?php echo $google; ?></a></li>
        </ul>
        <?php
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['text'] = $new_instance['text'];
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['link_fb'] = strip_tags($new_instance['link_fb']);
        $instance['fb'] = strip_tags($new_instance['fb']);
        $instance['link_twitter'] = strip_tags($new_instance['link_twitter']);
        $instance['twitter'] = strip_tags($new_instance['twitter']);
        $instance['link_google'] = strip_tags($new_instance['link_google']);
        $instance['google'] = strip_tags($new_instance['google']);
        return $instance;
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => ''));
        $title = strip_tags($instance['title']);
        $text = esc_textarea($instance['text']);
        $link_fb = esc_textarea($instance['link_fb']);
        $fb = esc_textarea($instance['fb']);
        $link_twitter = esc_textarea($instance['link_twitter']);
        $twitter = esc_textarea($instance['twitter']);
        $link_google = esc_textarea($instance['link_google']);
        $google = esc_textarea($instance['google']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

        <p><label for="<?php echo $this->get_field_id('fb'); ?>"><?php _e('Facebook:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('fb'); ?>" name="<?php echo $this->get_field_name('fb'); ?>" type="text" value="<?php echo esc_attr($fb); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('link_fb'); ?>"><?php _e('Link Facebook:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_fb'); ?>" name="<?php echo $this->get_field_name('link_fb'); ?>" type="text" value="<?php echo esc_attr($link_fb); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e('Twitter:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="text" value="<?php echo esc_attr($twitter); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('link_twitter'); ?>"><?php _e('Link Twitter:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_twitter'); ?>" name="<?php echo $this->get_field_name('link_twitter'); ?>" type="text" value="<?php echo esc_attr($link_twitter); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('google'); ?>"><?php _e('Google:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('google'); ?>" name="<?php echo $this->get_field_name('google'); ?>" type="text" value="<?php echo esc_attr($google); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('link_google'); ?>"><?php _e('Link Google:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_google'); ?>" name="<?php echo $this->get_field_name('link_google'); ?>" type="text" value="<?php echo esc_attr($link_google); ?>" /></p>
        <?php
    }

}

/**
 * Text widget class
 *
 * @since 2.8.0
 */
class Jobskills_Widget_FollowUs_Footer extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML.'));
        parent::__construct('followus_footer', __('Follow us Footer Jobskills'), $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        $link_fb = apply_filters('widget_link_fb', empty($instance['link_fb']) ? '' : $instance['link_fb'], $instance);
        $link_twitter = apply_filters('widget_link_twitter', empty($instance['link_twitter']) ? '' : $instance['link_twitter'], $instance);
        $link_google = apply_filters('widget_link_google', empty($instance['link_google']) ? '' : $instance['link_google'], $instance);
        $phone = apply_filters('widget_phone', empty($instance['phone']) ? '' : $instance['phone'], $instance);
        $workday_time = apply_filters('widget_workday_time', empty($instance['workday_time']) ? '' : $instance['workday_time'], $instance);
        $email = apply_filters('widget_email', empty($instance['email']) ? '' : $instance['email'], $instance);

        echo $before_widget;
        if (!empty($title)) {
            echo $before_title . $title . $after_title;
        }
        ?>
        <ul id="follow_us">
            <li><a href="<?php echo $link_fb; ?>"><i class="icon-facebook"></i></a></li>
            <li><a href="<?php echo $link_twitter; ?>"><i class="icon-twitter"></i></a></li>
            <li><a href="<?php echo $link_google; ?>"><i class="icon-google"></i></a></li>
        </ul>
        <ul>
            <li><strong class="phone"><?php echo $phone; ?></strong><br><small><?php echo $workday_time; ?></small></li>
            <li><?php __('Questions?', THEMENAME); ?> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></li>
        </ul>
        <?php
        echo $after_widget;
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['link_fb'] = strip_tags($new_instance['link_fb']);
        $instance['phone'] = strip_tags($new_instance['phone']);
        $instance['link_twitter'] = strip_tags($new_instance['link_twitter']);
        $instance['workday_time'] = strip_tags($new_instance['workday_time']);
        $instance['link_google'] = strip_tags($new_instance['link_google']);
        $instance['email'] = strip_tags($new_instance['email']);
        return $instance;
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => ''));
        $title = strip_tags($instance['title']);
        $link_fb = esc_textarea($instance['link_fb']);
        $phone = esc_textarea($instance['phone']);
        $link_twitter = esc_textarea($instance['link_twitter']);
        $workday_time = esc_textarea($instance['workday_time']);
        $link_google = esc_textarea($instance['link_google']);
        $email = esc_textarea($instance['email']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id('link_fb'); ?>"><?php _e('Link Facebook:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_fb'); ?>" name="<?php echo $this->get_field_name('link_fb'); ?>" type="text" value="<?php echo esc_attr($link_fb); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('link_twitter'); ?>"><?php _e('Link Twitter:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_twitter'); ?>" name="<?php echo $this->get_field_name('link_twitter'); ?>" type="text" value="<?php echo esc_attr($link_twitter); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('link_google'); ?>"><?php _e('Link Google:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_google'); ?>" name="<?php echo $this->get_field_name('link_google'); ?>" type="text" value="<?php echo esc_attr($link_google); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('workday_time'); ?>"><?php _e('Work Date Time:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('workday_time'); ?>" name="<?php echo $this->get_field_name('workday_time'); ?>" type="text" value="<?php echo esc_attr($workday_time); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" /></p>
        <?php
    }

}
