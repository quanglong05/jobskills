<?php

add_shortcode('main_features', 'main_features_shortcode');
add_shortcode('main_content_gray', 'main_content_gray_shortcode');
add_shortcode('main_content', 'main_content_shortcode');
add_shortcode('testimonial', 'testimonial_shortcode');

/**
 * load shortcode template with assigned data
 */
function sm_load_shortcode_template($templateName = '', $atts = array(), $data = array()) {
    if ($data && is_array($data)) {
        // extract data to variables
        extract($data);
    }
    // get template path
    $template = THEMEDIR . '/page_templates/' . $templateName;
    ob_start();
    if (file_exists($template)) {
        include $template;
    }
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

function main_features_shortcode($atts, $content = null) {
    $main_features = get_main_features_post($atts['limit']);
    return sm_load_shortcode_template('home/main_features.php', array(), array('main_features' => $main_features, 'content' => $content));
}

/**
 * main_features display homepage
 *
 * @param int $numberposts
 */
function get_main_features_post($numberposts = 4) {
    $args = array(
        'post_type' => 'main_features',
        'hierarchical' => 1,
        'post_status' => 'publish',
        'posts_per_page' => $numberposts,
    );
    $main_features = get_posts($args);
    return $main_features;
}

function main_content_gray_shortcode($atts, $content = null) {
    $main_content_grays = get_main_content_gray_post($atts['limit']);
    return sm_load_shortcode_template('home/main_content_gray.php', array(), array('main_content_grays' => $main_content_grays, 'content' => $content));
}

/**
 * main_content_gray display homepage
 *
 * @param int $numberposts
 */
function get_main_content_gray_post($numberposts = 4) {
    $args = array(
        'post_type' => 'main_content_gray',
        'hierarchical' => 1,
        'post_status' => 'publish',
        'posts_per_page' => $numberposts,
    );
    $main_content_grays = get_posts($args);
    return $main_content_grays;
}

function main_content_shortcode($atts, $content = null) {
    $main_contents = get_main_content_post($atts['limit']);
    return sm_load_shortcode_template('home/main_content.php', array(), array('main_contents' => $main_contents, 'content' => $content));
}

/**
 * main_content display homepage
 *
 * @param int $numberposts
 */
function get_main_content_post($numberposts = 4) {
    $args = array(
        'post_type' => 'main_content',
        'hierarchical' => 1,
        'post_status' => 'publish',
        'posts_per_page' => $numberposts,
    );
    $main_contents = get_posts($args);
    return $main_contents;
}

function testimonial_shortcode($atts, $content = null) {
    $testimonials = get_testimonial_post($atts['limit']);
    return sm_load_shortcode_template('home/testimonial.php', array(), array('testimonials' => $testimonials, 'content' => $content));
}

/**
 * main_content display homepage
 *
 * @param int $numberposts
 */
function get_testimonial_post($numberposts = -1) {
    $args = array(
        'post_type' => 'testimonial',
        'hierarchical' => 1,
        'post_status' => 'publish',
        'posts_per_page' => $numberposts,
    );
    $testimonials = get_posts($args);
    return $testimonials;
}
