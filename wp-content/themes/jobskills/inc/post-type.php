<?php

//add_action( 'init', 'sm_theme_register_taxonomies' );
add_action('init', 'sm_theme_custom_post_type');

/**
 * Register taxonomies use in theme
 */
function sm_theme_register_taxonomies() {
    // Register taxonomy team group
    register_taxonomy('category-resource', array('resource'), array(
        'hierarchical' => true, // Hierarchical taxonomy (like categories)
        // This array of options controls the labels displayed in the WordPress Admin UI
        'labels' => array(
            'name' => _x('Categories', THEMENAME),
            'singular_name' => _x('Categories', THEMENAME),
            'search_items' => __('Search Category', THEMENAME),
            'all_items' => __('All Categories', THEMENAME),
            'parent_item' => __('Parent Category', THEMENAME),
            'parent_item_colon' => __('Parent Category:', THEMENAME),
            'edit_item' => __('Edit Category', THEMENAME),
            'update_item' => __('Update Category', THEMENAME),
            'add_new_item' => __('Add New Category', THEMENAME),
            'new_item_name' => __('New Report Category', THEMENAME),
            'menu_name' => __('Categories', THEMENAME),
        ),
        // Control the slugs used for this taxonomy
        'rewrite' => array(
            'slug' => 'category-resource', // This controls the base slug that will display before each term
            'with_front' => false, // Don't display the category base before "/locations/"
            'hierarchical' => true, // This will allow URL's like "/locations/boston/cambridge/"
        ),
            )
    );
}

/**
 * Register a custom post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function sm_theme_custom_post_type() {
    /**
     * Register a main_features post type.
     */
    $main_features_labels = array(
        'name' => _x('Main Features', 'post type general name'),
        'singular_name' => _x('Main Feature', 'post type singular name'),
        'menu_name' => _x('Main Features', 'admin menu'),
        'name_admin_bar' => _x('Main Feature', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Main Feature'),
        'new_item' => __('New Main Feature'),
        'edit_item' => __('Edit Main Feature'),
        'view_item' => __('View Main Feature'),
        'all_items' => __('All Main Features'),
        'search_items' => __('Search Main Feature'),
        'parent_item_colon' => __('Parent Main Feature:'),
        'not_found' => __('No Main Feature found.'),
        'not_found_in_trash' => __('No Main Feature found in Trash.'),
    );

    $main_features_args = array(
        'labels' => $main_features_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'main_features'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail', 'excerpt'),
    );

    register_post_type('main_features', $main_features_args);

    /**
     * Register a main_content_gray post type.
     */
    $main_content_gray_labels = array(
        'name' => _x('Main Content Gray', 'post type general name'),
        'singular_name' => _x('Main Content Gray', 'post type singular name'),
        'menu_name' => _x('Main Content Gray', 'admin menu'),
        'name_admin_bar' => _x('Main Content Gray', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Main Content Gray'),
        'new_item' => __('New Main Content Gray'),
        'edit_item' => __('Edit Main Content Gray'),
        'view_item' => __('View Main Content Gray'),
        'all_items' => __('All Main Content Gray'),
        'search_items' => __('Search Main Content Gray'),
        'parent_item_colon' => __('Parent Main Content Gray:'),
        'not_found' => __('No Main Content Gray found.'),
        'not_found_in_trash' => __('No Main Content Gray found in Trash.'),
    );

    $main_content_gray_args = array(
        'labels' => $main_content_gray_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'main_content_gray'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail', 'editor'),
    );

    register_post_type('main_content_gray', $main_content_gray_args);

    /**
     * Register a main_content_gray post type.
     */
    $main_content_labels = array(
        'name' => _x('Main Contents', 'post type general name'),
        'singular_name' => _x('Main Contents', 'post type singular name'),
        'menu_name' => _x('Main Contents', 'admin menu'),
        'name_admin_bar' => _x('Main Contents', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Main Conten'),
        'new_item' => __('New Main Content'),
        'edit_item' => __('Edit Main Content'),
        'view_item' => __('View Main Content'),
        'all_items' => __('All Main Content'),
        'search_items' => __('Search Main Content'),
        'parent_item_colon' => __('Parent Main Content:'),
        'not_found' => __('No Main Content found.'),
        'not_found_in_trash' => __('No Main Content found in Trash.'),
    );

    $main_content_args = array(
        'labels' => $main_content_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'main_content'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail', 'editor'),
    );

    register_post_type('main_content', $main_content_args);

    /**
     * Register a testimonial post type.
     */
    $testimonial_labels = array(
        'name' => _x('Testimonials', 'post type general name'),
        'singular_name' => _x('Testimonials', 'post type singular name'),
        'menu_name' => _x('Testimonials', 'admin menu'),
        'name_admin_bar' => _x('Testimonials', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Testimonial'),
        'new_item' => __('New Testimonial'),
        'edit_item' => __('Edit Testimonial'),
        'view_item' => __('View Testimonial'),
        'all_items' => __('All Testimonials'),
        'search_items' => __('Search Testimonial'),
        'parent_item_colon' => __('Parent Testimonial:'),
        'not_found' => __('No Testimonial found.'),
        'not_found_in_trash' => __('No Testimonial found in Trash.'),
    );

    $testimonial_args = array(
        'labels' => $testimonial_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'testimonial'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail', 'excerpt')
    );

    register_post_type('testimonial', $testimonial_args);
}