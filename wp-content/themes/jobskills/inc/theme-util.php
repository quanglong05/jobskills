<?php

require_once THEMEDIR . '/class/IC/Security.php';

function jobskills_disable_url() {
    return '#';
}

/**
 * Add ajax action in wordpress
 * @param object $function
 * @param string $callback
 */
function add_ajax_function($function, $callback) {
    $arrAjaxType = array(
        'wp_ajax_',
        'wp_ajax_nopriv_',
    );

    foreach ($arrAjaxType as $type) {
        add_action($type . $function, $callback);
    }
}

/**
 * Get meta of image
 * @param object $function
 * @param string $callback
 */
function wp_get_attachment($attachment_id) {

    $attachment = get_post($attachment_id);
    return array(
        'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink($attachment->ID),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Fourteen 1.0
 */
function jobskills_paging_nav() {
    // Don't print empty markup if there's only one page.
    if ($GLOBALS['wp_query']->max_num_pages < 2) {
        return;
    }

    $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
    $pagenum_link = html_entity_decode(get_pagenum_link());
    $query_args = array();
    $url_parts = explode('?', $pagenum_link);

    if (isset($url_parts[1])) {
        wp_parse_str($url_parts[1], $query_args);
    }

    $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
    $pagenum_link = trailingslashit($pagenum_link) . '%_%';

    $format = $GLOBALS['wp_rewrite']->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
    $format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit('page/%#%', 'paged') : '?paged=%#%';

    // Set up paginated links.
    $links = paginate_links(array(
        'base' => $pagenum_link,
        'format' => $format,
        'total' => $GLOBALS['wp_query']->max_num_pages,
        'current' => $paged,
        'mid_size' => 1,
        'add_args' => array_map('urlencode', $query_args),
        'prev_text' => __('Prev', THEMENAME),
        'next_text' => __('Next', THEMENAME),
        'type' => 'list',
    ));

    if ($links) :
?>
        <?php echo $links; ?>
        <?php

    endif;
}

