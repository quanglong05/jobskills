<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header();
?>
<section id="main_content">
    <div class="container">
        <?php breadcrumb_trail(); ?>
        <div class="row">
            <?php get_sidebar(); ?>
            <div class="col-md-8">
                <?php if (have_posts()) : ?>
                    <?php
                    // Start the Loop.
                    while (have_posts()) :
                        the_post();
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog_category_list');
                        ?>
                        <div class="post">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo $image[0] ?>" alt="" class="img-responsive"></a>
                            <div class="post_info clearfix">
                                <div class="post-left">
                                    <ul>
                                        <li><i class="icon-calendar-empty"></i><?php echo __("On", THEMENAME); ?> <span><?php echo get_the_date('d M Y'); ?></span></li>
                                        <li><i class="icon-user"></i><?php echo __("By", THEMENAME); ?> <a href="<?php echo get_the_author_link(); ?>"><?php the_author(); ?></a></li>
                                        <li>
                                            <i class="icon-tags"></i>
                                            <?php echo __("Tags", THEMENAME); ?>
                                            <?php
                                            $args = array(
                                                'taxonomy' => 'tags',
                                                'unit' => 'px',
                                                'smallest' => 13,
                                            );
                                            echo wp_generate_tag_cloud(get_the_tags(), $args);
                                            ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                            <p>
                                <?php echo wp_trim_words($post->post_content, 50, '...'); ?>
                            </p>
                            <a href="<?php the_permalink(); ?>" class="button_medium" title="<?php echo __("Read more", THEMENAME); ?>"><?php echo __("Read more", THEMENAME); ?></a>
                        </div><!-- end post -->
                        <?php
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
                <hr>
                <div class="text-center">
                    <?php jobskills_paging_nav() ?>
                </div>
            </div><!-- End col-md-8-->
        </div>  <!-- End row-->
    </div><!-- End container -->
</section><!-- End main_content-->
<?php get_footer(); ?>