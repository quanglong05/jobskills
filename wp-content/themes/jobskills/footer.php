<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?php
                echo apply_filters('the_content', '[sbscrbr_form]');
                ?>
            </div>
        </div>
    </div>
    <hr>
    <?php
    echo get_sidebar('footer');
    ?>
    <div id="copy_right">© 1998-2014</div>
</footer>
<div id="toTop">Back to top</div>
<!-- JQUERY -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function() {
        revapi = jQuery('.tp-banner').revolution(
                {
                    delay: 9000,
                    startwidth: 1170,
                    startheight: 500,
                    hideThumbs: true,
                    navigationType: "none",
                    fullWidth: "on",
                    forceFullWidth: "on"
                });
    });	//ready
</script>
<!-- OTHER JS -->
<script src="<?php echo get_template_directory_uri(); ?>/js/superfish.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/retina.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/validate.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.placeholder.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/uisearch.js"></script>
<script>new UISearch(document.getElementById('sb-search'));</script>
<?php wp_footer(); ?>
</body>
</html>