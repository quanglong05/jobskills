<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>

<section id="main_content">

    <div class="container">
        <div class="row">
            <?php get_sidebar(); ?><!-- End aside -->
            <div class="col-md-8">
                <div class="post">
                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post();
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog_category_list');
                        ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo $image[0] ?>" alt="" class="img-responsive"></a>
                        <div class="post_info clearfix">
                            <div class="post-left">
                                <ul>
                                    <li><i class="icon-calendar-empty"></i><?php echo __("On", THEMENAME); ?> <span><?php echo get_the_date('d M Y'); ?></span></li>
                                    <li><i class="icon-user"></i><?php echo __("By", THEMENAME); ?> <a href="<?php echo get_the_author_link(); ?>"><?php the_author(); ?></a></li>
                                    <li>
                                        <i class="icon-tags"></i>
                                        <?php echo __("Tags", THEMENAME); ?> 
                                        <?php
                                        $args = array(
                                            'taxonomy' => 'tags',
                                            'unit' => 'px',
                                            'smallest' => 14,
                                        );
                                        echo wp_generate_tag_cloud( get_the_tags(), $args );
                                        ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title() ?>"><?php the_title() ?></a></h2>
                        <?php the_content() ?>
                        <?php
                    endwhile;
                    ?>
                </div><!-- end post -->
            </div><!-- End col-md-8-->   
        </div>  <!-- End row-->    
    </div><!-- End container -->
</section><!-- End main_content-->

<?php
get_footer();
