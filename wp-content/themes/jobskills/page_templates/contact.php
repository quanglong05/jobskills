<?php
/**
 * Template Name: Contact Page
 */
get_header();
?>
<section id="map">
    <?php echo apply_filters('the_content', '[wpgmza id="1"]'); ?>
</section><!-- end map-->
<section id="main_content" >
    <div class="container">
        <div class="row">
            <?php
            echo get_sidebar('contact');
            ?>
            <div class="col-md-8">
                <div class=" box_style_2">
                    <?php
                    // Start the Loop.
                    while (have_posts()) : the_post();
                        ?>
                        <span class="tape"></span>
                        <div class="row">
                            <div class="col-md-12">
                                <h3><?php the_title(); ?></h3>
                            </div>
                        </div>
                        <div id="message-contact"></div>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
