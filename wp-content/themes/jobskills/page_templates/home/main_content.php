<section id="main_content">
    <div class="container">
        <div class="row add_bottom_30">
            <div class="col-md-12 text-center">
                <?php echo $content; ?>
            </div>
        </div>
        <div class="row">
            <?php
            $count = 0;
            foreach ($main_contents as $main_content) :
                ?>
                <?php if ($count % 2 == 0): ?>
                    <div class="col-md-6">
                    <?php endif; ?>
                    <div class="media">
                        <div class="circ-wrapper pull-left"><h3><?php echo get_the_date('d', $main_content->ID); ?><br><?php echo get_the_date('M', $main_content->ID); ?></h3></div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="<?php echo get_page_link($main_content->ID); ?>"><?php echo $main_content->post_title; ?></a></h4>
                            <p><?php echo wp_trim_words($main_content->post_content, 20, '...'); ?></p>
                        </div>
                    </div>
                    <?php if (($count % 2) == 1) : ?>
                    </div>
                <?php endif; ?>
                <?php $count++; ?>
            <?php endforeach; ?>
            <?php if (($count % 2) != 0) : ?>
            </div>
        <?php endif; ?>
    </div><!-- End row -->
</div><!-- End container -->
</section><!-- End main_content -->