<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class='col-md-offset-2 col-md-8 text-center'>
                <?php echo $content ?>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-offset-2 col-md-8'>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <?php for ($i == 0; $i < count($testimonials); $i++) { ?>
                            <li data-target="#quote-carouse<?php echo $i ?>" data-slide-to="<?php echo $i ?>" <?php echo ($i == 0) ? 'class="active"' : '' ?>></li>
                            <?php
                        }
                        ?>                  
                    </ol>
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">
                        <!-- Quote 1 -->
                        <?php
                        $count = 0;
                        foreach ($testimonials as $testimonial) :
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($testimonial->ID), 'testimonial_image');
                            ?>
                            <div class="item <?php echo ($count == 0) ? 'active' : '' ?>">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-3 text-center">
                                            <img class="img-circle" src="<?php echo $image[0]; ?>" alt="">
                                        </div>
                                        <div class="col-sm-9">
                                            <p>
                                                <?php echo $testimonial->post_excerpt; ?>
                                            </p>
                                            <small><?php echo $testimonial->post_title; ?></small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <?php $count++ ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>