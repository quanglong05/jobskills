<section id="main-features">
    <div class="divider_top_black"></div>
    <div class="container">
        <div class="row">
            <div class=" col-md-10 col-md-offset-1 text-center">
                <?php echo $content; ?>
            </div>
        </div>
        <?php if ($main_features) : ?>
            <div class="row">
                <?php foreach ($main_features as $main) : ?>
                    <div class="col-md-6">
                        <div class="feature">
                            <i class="icon-trophy"></i>
                            <h3><?php echo $main->post_title ?></h3>
                            <p>
                                <?php echo $main->post_excerpt ?>
                            </p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div><!-- End row -->
        <?php endif; ?>
    </div><!-- End container-->
</section><!-- End main-features -->