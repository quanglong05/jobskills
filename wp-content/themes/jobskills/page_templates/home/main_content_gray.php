<section id="main_content_gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php echo $content; ?>
            </div>
        </div><!-- End row -->
        <div class="row">
            <?php
            foreach ($main_content_grays as $main_content) :
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($main_content->ID), 'main_content_gray_image');
                $user_info = get_users($main_content->post_author);
                ?>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="col-item">
                        <span class="ribbon_course"></span>
                        <div class="photo">
                            <a href="<?php echo get_page_link($main_content->ID); ?>"><img src="<?php echo $image[0]; ?>" alt="" /></a>
                            <div class="cat_row"></div>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="course_info col-md-12 col-sm-12">
                                    <h4><?php echo $main_content->post_title; ?></h4>
                                    <p><?php echo wp_trim_words($main_content->post_content, 30, '...'); ?></p>
                                </div>
                            </div>
                            <div class="separator clearfix">
                                <p> <a href="<?php echo get_page_link($main_content->ID); ?>"><i class=" icon-list"></i><?php echo __(" Details", THEMENAME); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!-- End row -->
    </div>   <!-- End container -->
</section><!-- End section gray -->