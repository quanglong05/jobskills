<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */
get_header()
?>
<section id="main_content">
    <div class="container">
        <div class="row">
            <?php
            // Start the Loop.
            while (have_posts()) : the_post();
                $right_column = $cfs->get('page_column_right', get_the_ID());
                ?>
                <div class="col-md-7">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
                <div class="col-md-4 col-md-offset-1">
                    <div class="row">
                        <?php
                        foreach ($right_column as $right) :
                            $image = wp_get_attachment_image_src($right["image"], 'page_right');
                            ?>
                            <div class="col-md-12 col-sm-6">
                                <div class="thumbnail">
                                    <div class="project-item-image-container">
                                        <img src="<?php echo $image[0]; ?>" alt=""/>
                                    </div>
                                    <div class="caption">
                                        <div class="transit-to-top">
                                            <h4 class="p-title"><?php echo $right["title"]; ?></h4>
                                            <div class="widget_nav_menu">
                                                <ul class="social_team">
                                                    <li><a href="<?php echo $right["facebook_link"]; ?>"><i class="icon-facebook"></i></a></li>
                                                    <li><a href="<?php echo $right["twitter_link"]; ?>"><i class="icon-twitter"></i></a></li>
                                                    <li><a href="<?php echo $right["google_link"]; ?>"><i class=" icon-google"></i></a></li>
                                                </ul>
                                                <div class="phone-info">
                                                    <i class=" icon-phone"></i> <?php echo $right["phone"]; ?>
                                                </div>
                                            </div><!-- transition top -->
                                        </div><!-- caption -->
                                    </div>
                                </div><!-- End col-md-12-->
                            </div><!-- End thumbnail -->
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php
            endwhile;
            ?>
        </div><!-- End row -->
    </div><!-- End container -->
</section><!-- End main_content-->
<?php
get_footer();


