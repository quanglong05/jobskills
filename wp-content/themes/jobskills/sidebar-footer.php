<?php
/**
 * The Footer Sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

if ( ! is_active_sidebar( 'sidebar-3' ) ) {
	return;
}
?>
<div class="container" id="nav-footer">
    <div class="row text-left">
        <?php dynamic_sidebar( 'sidebar-3' ); ?>
    </div>
</div>