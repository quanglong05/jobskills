<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<aside class="col-md-4">
    <div class=" box_style_1">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div>
</aside>