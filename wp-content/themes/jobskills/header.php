<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <title><?php wp_title('|', true, 'right'); ?></title>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- Favicons-->
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon"/>
        <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-144x144-precomposed.png">
        <!-- CSS -->
        <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/css/superfish.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/fontello/css/fontello.css" rel="stylesheet">
        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link href="<?php echo get_template_directory_uri(); ?>/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <a href="index-2.html" id="logo">Learn</a>
                    </div>
                </div>
            </div>
        </header><!-- End header -->

        <nav>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="mobnav-btn"></div>
                        <?php
                        $locations = get_nav_menu_locations();
                        wp_nav_menu( array( 'menu' => $locations[ 'primary' ], 'menu_class' => 'sf-menu', 'container' => '', 'walker' => new Jobskills_LBMenuWalker() ) );
                        ?>
                        <div class="col-md-4 pull-right hidden-sm hidden-xs">
                            <div id="sb-search" class="sb-search">
                                <?php //get_search_form(); ?>
                            </div>
                        </div>
                        <!-- End search -->
                    </div>
                </div><!-- End row -->
            </div><!-- End container -->
        </nav>