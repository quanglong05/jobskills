<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
    <div class="input-group">
        <input type="text" name="s" id="s" class="form-control" placeholder="<?php echo esc_attr_x( 'Search...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>">
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit" style="margin-left:0;"><i class="icon-search"></i></button>
        </span>
    </div>
</form>

