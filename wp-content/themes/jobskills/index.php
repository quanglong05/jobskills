<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header();
?>

<section class="tp-banner-container">
    <div class="tp-banner" >
        <ul>	<!-- SLIDE  -->
            <li data-transition="fade" data-slotamount="4" data-masterspeed="1500" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo get_template_directory_uri(); ?>/images/sliderimages/slide_5.jpg" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption skewfromrightshort customout"
                     data-x="center"
                     data-y="center"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1200"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 2"><img src="<?php echo get_template_directory_uri(); ?>/images/sliderimages/logo.png" alt="">
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="left"
                     data-y="190"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1500"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">PHP Working Subscription
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="left"
                     data-y="245"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1500"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">PHP Working wizard
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="left"
                     data-y="300"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1500"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">PHP Working newsletter
                </div>
                <!-- LAYER NR. 4 -->
                <div class="tp-caption medium_bg_darkblue skewfromright customout"
                     data-x="right"
                     data-y="190"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Four Home pages
                </div>
                <!-- LAYER NR. 5 -->
                <div class="tp-caption medium_bg_darkblue skewfromright customout"
                     data-x="right"
                     data-y="245"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Course timeline
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="right"
                     data-y="300"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">20 HTML pages
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="zoomout" data-slotamount="4" data-masterspeed="1000" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo get_template_directory_uri(); ?>/images/sliderimages/slide_6.jpg" alt="slidebg2"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption medium_light_white skewfromrightshort customout"
                     data-x="80"
                     data-y="96"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="800"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 4">Enjoy
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_thin_white skewfromleftshort customout"
                     data-x="235"
                     data-y="110"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="900"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 5">&
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption large_bold_white skewfromleftshort customout"
                     data-x="80"
                     data-y="152"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="1100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 7">Share your skills
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption small_thin_white  customin customout"
                     data-x="80"
                     data-y="240"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Slider Revolution is the highly acclaimed<br/> slide-based displaying solution, thousands of<br/> businesses, theme developers and everyday<br/> people use and love!
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="cube-horizontal" data-slotamount="4" data-masterspeed="1000" >
                <!-- MAIN IMAGE -->
                <img src="<?php echo get_template_directory_uri(); ?>/images/sliderimages/slide_1.jpg" alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption medium_light_white skewfromrightshort customout"
                     data-x="80"
                     data-y="96"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="800"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 4">Enjoy
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_thin_white skewfromleftshort customout"
                     data-x="235"
                     data-y="110"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="900"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 5">&
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption large_bold_white skewfromleftshort customout"
                     data-x="80"
                     data-y="152"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="1100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 7">Improve your skills
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption small_thin_white  customin customout"
                     data-x="80"
                     data-y="240"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Slider Revolution is the highly acclaimed<br/> slide-based displaying solution, thousands of<br/> businesses, theme developers and everyday<br/> people use and love!
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="zoomin" data-slotamount="4" data-masterspeed="1000" >
                <!-- MAIN IMAGE -->
                <img src="sliderimages/slide_7.jpg" alt="slidebg4"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 10 -->
                <div class="tp-caption large_bold_white customin customout"
                     data-x="center" data-hoffset="0"
                     data-y="160"
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="1000"
                     data-start="1700"
                     data-easing="Back.easeInOut"
                     data-endspeed="300"
                     style="z-index: 11">Single or multi course
                </div>

                <!-- LAYER NR. 11 -->
                <div class="tp-caption mediumlarge_light_white_center customin customout"
                     data-x="center" data-hoffset="0"
                     data-y="bottom" data-voffset="-150"
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="1000"
                     data-start="1900"
                     data-easing="Back.easeInOut"
                     data-endspeed="300"
                     style="z-index: 12">Lorem ipsum dolor sit amet,<br/>
                    civibus efficiantur in<br/>
                    id tempor imperdiet deterruisse.
                </div>
            </li>

        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</section><!-- End slider -->
<section id="main-features">
    <div class="divider_top_black"></div>
    <div class="container">
        <div class="row">
            <div class=" col-md-10 col-md-offset-1 text-center">
                <h2>Why Join Learn</h2>
                <p class="lead">
                    Lorem ipsum dolor sit amet, ius minim gubergren ad. <br>
                    At mei sumo sonet audiam, ad mutat elitr platonem vix. Ne nisl idque fierent vix. Ferri clitaponderum ne.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="feature">
                    <i class="icon-trophy"></i>
                    <h3>Expert teachers</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="feature">
                    <i class=" icon-ok-4"></i>
                    <h3>Trusted certifications</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="feature">
                    <i class="icon-mic"></i>
                    <h3>+500 Audio lessons</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="feature">
                    <i class="icon-video"></i>
                    <h3>+1.200 Video lessons</h3>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
                    </p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container-->
</section><!-- End main-features -->
<section id="main_content_gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Latest Courses</h2>
                <p class="lead">Lorem ipsum dolor sit amet, ius minim gubergren ad.</p>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="col-item">
                    <span class="ribbon_course"></span>
                    <div class="photo">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/poetry.jpg" alt="" /></a>
                        <div class="cat_row"><a href="#">LITERATURE</a><span class="pull-right"><i class=" icon-clock"></i>6 Days</span></div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="course_info col-md-12 col-sm-12">
                                <h4>Poetry course</h4>
                                <p > Lorem ipsum dolor sit amet, no sit sonet corpora indoctum, quo ad fierent insolens. Duo aeterno ancillae ei. </p>
                                <div class="rating">
                                    <i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class=" icon-star-empty"></i>
                                </div>
                            </div>
                        </div>
                        <div class="separator clearfix">
                            <p class="btn-add"> <a href="apply_2.html"><i class="icon-export-4"></i> Subscribe</a></p>
                            <p class="btn-details"> <a href="course_details_1.html"><i class=" icon-list"></i> Details</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="col-item">
                    <div class="photo">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/business.jpg" alt="" /></a>
                        <div class="cat_row"><a href="#">MANAGEMENT</a><span class="pull-right"><i class=" icon-clock"></i>6 Days</span></div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="course_info col-md-12 col-sm-12">
                                <h4>Build a Business Plan</h4>
                                <p > Lorem ipsum dolor sit amet, no sit sonet corpora indoctum, quo ad fierent insolens. Duo aeterno ancillae ei. </p>
                                <div class="rating">
                                    <i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class=" icon-star-empty"></i><i class=" icon-star-empty"></i>
                                </div>
                            </div>
                        </div>
                        <div class="separator clearfix">
                            <p class="btn-add"> <a href="apply_2.html"><i class="icon-export-4"></i> Subscribe</a></p>
                            <p class="btn-details"> <a href="course_details_1.html"><i class=" icon-list"></i> Details</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="col-item">
                    <div class="photo">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/art.jpg" alt="" /></a>
                        <div class="cat_row"><a href="#">ART</a><span class="pull-right"><i class=" icon-clock"></i>6 Days</span></div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="course_info col-md-12 col-sm-12">
                                <h4>Impressionist</h4>
                                <p > Lorem ipsum dolor sit amet, no sit sonet corpora indoctum, quo ad fierent insolens. Duo aeterno ancillae ei. </p>
                                <div class="rating">
                                    <i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="separator clearfix">
                            <p class="btn-add"> <a href="apply_2.html"><i class="icon-export-4"></i> Subscribe</a></p>
                            <p class="btn-details"> <a href="course_details_1.html"><i class=" icon-list"></i> Details</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="col-item">
                    <div class="photo">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/math.jpg" alt="" /></a>
                        <div class="cat_row"><a href="#">MATH</a><span class="pull-right"><i class=" icon-clock"></i>6 Days</span></div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="course_info col-md-12 col-sm-12">
                                <h4>12 Principles</h4>
                                <p > Lorem ipsum dolor sit amet, no sit sonet corpora indoctum, quo ad fierent insolens. Duo aeterno ancillae ei. </p>
                                <div class="rating">
                                    <i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class=" icon-star-empty"></i><i class=" icon-star-empty"></i>
                                </div>
                            </div>
                        </div>
                        <div class="separator clearfix">
                            <p class="btn-add"> <a href="apply_2.html"><i class="icon-export-4"></i> Subscribe</a></p>
                            <p class="btn-details"> <a href="course_details_1.html"><i class=" icon-list"></i> Details</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <a href="courses_grid.html" class="button_medium_outline pull-right">View all courses</a>
            </div>
        </div>
    </div>   <!-- End container -->
</section><!-- End section gray -->
<section id="main_content">
    <div class="container">
        <div class="row add_bottom_30">
            <div class="col-md-12 text-center">
                <h2>NextCourses</h2>
                <p class="lead">Lorem ipsum dolor sit amet, ius minim gubergren ad.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="media">
                    <div class="circ-wrapper pull-left"><h3>25<br>June</h3></div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="javascript:void(0)">CSS3 Editable Flat effect</a> <span class="label label-warning">NEW</span></h4>
                        <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p>
                        <ul class="data-lessons">
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-clock"></i>Duration: 6 hours</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Duration: 6 hours</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-video"></i>Video files</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Video files</strong></div> <!-- ./po-title -->
                                    <div class="po-body"><ul class="list_po_body"><li><i class="icon-ok"></i> 2 Video lessons</li><li> <i class="icon-ok"></i>1 Video for practice</li><li> <i class="icon-ok"></i>1 Video Quiz</li></ul>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-mic"></i>Audio files</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Audio files</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="media">
                    <div class="circ-wrapper pull-left"><h3>15<br>July</h3></div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="javascript:void(0)">CSS3 Workshop</a> <span class="label label-warning">NEW</span></h4>
                        <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p> 
                        <ul class="data-lessons">
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-clock"></i>Duration: 6 hours</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Duration: 6 hours</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-video"></i>Video files</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Video files</strong></div> <!-- ./po-title -->
                                    <div class="po-body"><ul class="list_po_body"><li><i class="icon-ok"></i> 2 Video lessons</li><li> <i class="icon-ok"></i>1 Video for practice</li><li> <i class="icon-ok"></i>1 Video Quiz</li></ul>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-doc-text-inv"></i>Text doc</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Text doc</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- End col md 6 -->
            <div class="col-md-6">
                <div class="media">
                    <div class="circ-wrapper pull-left"><h3>12<br>Aug</h3></div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="javascript:void(0)">CSS3 Editable Flat effect</a></h4>
                        <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p>
                        <ul class="data-lessons">
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-clock"></i>Duration: 6 hours</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Duration: 6 hours</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-video"></i>Video files</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Video files</strong></div> <!-- ./po-title -->
                                    <div class="po-body"><ul class="list_po_body"><li><i class="icon-ok"></i> 2 Video lessons</li><li> <i class="icon-ok"></i>1 Video for practice</li><li> <i class="icon-ok"></i>1 Video Quiz</li></ul>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-mic"></i>Audio files</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Audio files</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="media">
                    <div class="circ-wrapper pull-left"><h3>25<br>Sept</h3></div>
                    <div class="media-body" >
                        <h4 class="media-heading"><a href="javascript:void(0)">CSS3 Workshop</a></h4>
                        <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p>
                        <ul class="data-lessons">
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-clock"></i>Duration: 6 hours</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Duration: 6 hours</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-video"></i>Video files</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Video files</strong></div> <!-- ./po-title -->
                                    <div class="po-body"><ul class="list_po_body"><li><i class="icon-ok"></i> 2 Video lessons</li><li> <i class="icon-ok"></i>1 Video for practice</li><li> <i class="icon-ok"></i>1 Video Quiz</li></ul>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                            <li class="po-markup">
                                <a class="po-link" href="javascript:void(0)" ><i class="icon-doc-text-inv"></i>Text doc</a>
                                <div class="po-content hidden">
                                    <div class="po-title"><strong>Text doc</strong></div> <!-- ./po-title -->
                                    <div class="po-body">
                                        <p class="no_margin">Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.</p>
                                    </div><!-- ./po-body -->
                                </div><!-- ./po-content -->
                            </li>
                        </ul> 
                    </div>
                </div>

            </div>
        </div><!-- End row -->
        <hr>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h4>Our team</h4>
                <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_1.jpg" alt="Pic" class="img-responsive"></p>
                <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p> 
            </div>

            <div class="col-md-3 col-sm-6">
                <h4>Equiped classrooms</h4>
                <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_2.jpg" alt="Pic" class="img-responsive"></p>
                <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p> 
            </div>

            <div class="col-md-3 col-sm-6">
                <h4>Enjoy classroom mates</h4>
                <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_3.jpg" alt="Pic" class="img-responsive"></p>
                <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.</p> 
            </div>

            <div class="col-md-3 col-sm-6">
                <h4>Links</h4>
                <ul class="list_1">
                    <li><a href="#">Ceteros mediocritatem</a></li>
                    <li><a href="#">Labore nostrum</a></li>
                    <li><a href="#">Primis bonorum</a></li>
                    <li><a href="#">Ceteros mediocritatem</a></li>
                    <li><a href="#">Labore nostrum</a></li>
                    <li><a href="#">Primis bonorum</a></li>
                </ul>
            </div>

        </div><!-- End row -->

        <hr class="add_bottom_30">
        <div class="row">
            <div class="col-md-12">
                <p><img src="<?php echo get_template_directory_uri(); ?>/images/sponsors.jpg" alt="Pic" class="img-responsive"></p>
            </div>
        </div>
    </div><!-- End container -->
</section><!-- End main_content -->

<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class='col-md-offset-2 col-md-8 text-center'>
                <h2>What they say</h2>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-offset-2 col-md-8'>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#quote-carousel" data-slide-to="1"></li>
                        <li data-target="#quote-carousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/images/testimonial_1.jpg" alt="">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>
                                            Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!
                                        </p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/images/testimonial_2.jpg" alt="">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.
                                        </p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/images/testimonial_1.jpg" alt="">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                        </p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End testimonials -->
<?php get_footer(); ?>