<?php

if ( !defined( 'ABSPATH' ) ) {
 exit; // Exit if accessed directly
}


/**
 * Get Http request
 */
if ( !class_exists( "IC_Request" ) ) {

 class IC_Request
 {

  public static function getParam( $key, $default = null )
  {
   if ( isset( $_REQUEST[ $key ] ) ) {
    return $_REQUEST[ $key ];
   }
   return $default;
  }

  public static function getParams()
  {
   if ( $_REQUEST ) {
    return $_REQUEST;
   }
   return array( );
  }

  public static function getPost( $key, $default = null )
  {
   if ( isset( $_POST[ $key ] ) ) {
    return $_POST[ $key ];
   }
   return $default;
  }

  public static function getPosts()
  {
   if ( $_POST ) {
    return $_POST;
   }
   return array( );
  }

  public static function isAjax()
  {
   if ( isset( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) && strtolower( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) == 'xmlhttprequest' ) {
    return true;
   }
   return false;
  }

 }

}