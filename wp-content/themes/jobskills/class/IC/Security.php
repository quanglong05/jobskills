<?php

if ( !defined( 'ABSPATH' ) ) {
 exit; // Exit if accessed directly
}


/**
 * Generate and validate token
 */
require_once 'Request.php';

class IC_Security
{

 protected static $_formToken = array( );
 protected static $_session = array( );

 public static function init()
 {
  if ( !session_id() ) {
   session_start();
  }
  if ( !empty( $_SESSION[ 'ic_security' ][ 'form_token' ] ) ) {
   self::$_session = $_SESSION[ 'ic_security' ][ 'form_token' ];
  }
 }

 public static function generateToken( $formName )
 {
  if ( isset( self::$_formToken[ $formName ] ) ) {
   //throw new Exception(sprintf(__('Token for %s already exists. Please use another form name'), $formName));
  }
  if ( empty( $formName ) ) {
   throw new Exception( __( 'Form name can not empty' ) );
  }
  $_SESSION[ 'ic_security' ][ 'form_token' ][ $formName ] = self::$_formToken[ $formName ] = self::_generateRandomString();
  $input = sprintf( '<input type="hidden" name="ic_form_token" value="%s" />', self::$_formToken[ $formName ] );
  return $input;
 }

 public static function validateFormToken( $formName )
 {
  if ( isset( self::$_session[ $formName ] ) && self::$_session[ $formName ] == IC_Request::getPost( 'ic_form_token' ) ) {
   return true;
  }
  return false;
 }

 protected static function _generateRandomString( $length = 32 )
 {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $characters = str_shuffle( $characters );
  return substr( $characters, 0, $length );
 }

}

IC_Security::init();
