<?php

// define theme text domain
define('THEMENAME', 'jobskills');
define('THEMEDIR', get_template_directory());

/**
 * Jobskills setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 * Jobskills supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Jobskills 1.0
 */
function jobskills_setup() {
    /*
     * Makes Twenty Twelve available for translation.
     *
     * Translations can be added to the /languages/ directory.
     * If you're building a theme based on Twenty Twelve, use a find and replace
     * to change 'twentytwelve' to the name of your theme in all the template files.
     */
    load_theme_textdomain(THEMENAME, get_template_directory() . '/languages');

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // This theme supports a variety of post formats.
    add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
    // This theme uses wp_nav_menu() in one location.
    register_nav_menu('primary', __('Primary Menu', THEMENAME));

    /*
     * This theme supports custom background color and image,
     * and here we also set up the default background color.
     */
    add_theme_support('custom-background', array(
        'default-color' => 'e6e6e6',
    ));

    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(624, 9999); // Unlimited height, soft crop
    add_image_size('blog_category_list', 950, 375, true);
    add_image_size('page_right_column', 700, 430, true);
    add_image_size('testimonial_image', 200, 200, true);
    add_image_size('main_content_gray_image', 800, 600, true);
}

add_action('after_setup_theme', 'jobskills_setup');
add_action('widgets_init', 'jobskills_widgets_init');

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 */
function jobskills_widgets_init() {
    require get_template_directory() . '/inc/widgets.php';
    register_widget( 'Jobskills_Widget');
    register_widget( 'Jobskills_Widget_Recent_Posts' );
    register_widget( 'Jobskills_Widget_Search' );
    register_widget( 'Jobskills_Tag_Cloud' );
    register_widget( 'Jobskills_Widget_Text' );
    register_widget( 'Jobskills_Widget_Address' );
    register_widget( 'Jobskills_Widget_FollowUs' );
    register_widget( 'Jobskills_Widget_FollowUs_Footer' );

    register_sidebar(array(
        'name' => __('Primary Sidebar', THEMENAME),
        'id' => 'sidebar-1',
        'description' => __('Main sidebar that appears on the left.', THEMENAME),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Content Sidebar', THEMENAME),
        'id' => 'sidebar-2',
        'description' => __('Additional sidebar that appears on the right.', THEMENAME),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Footer Widget Area', THEMENAME),
        'id' => 'sidebar-3',
        'description' => __('Appears in the footer section of the site.', THEMENAME),
        'before_widget' => '<div class="col-md-3 col-sm-3 %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => __('Contact Sidebar', THEMENAME),
        'id' => 'sidebar-4',
        'description' => __('Contact sidebar that appears on the left.', THEMENAME),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
}



//init theme support
require_once get_template_directory() . '/inc/theme-util.php';
require_once get_template_directory() . '/inc/post-type.php';
require_once get_template_directory() . '/inc/theme-shortcode.php';
require_once get_template_directory() . '/inc/breadcrumbs.php';
require_once get_template_directory() . '/inc/theme-widgets.php';
require_once get_template_directory() . '/class/Jobskills-LBMenuWalker.php';
