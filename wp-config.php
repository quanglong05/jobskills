<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jobskills');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ed123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`|}[1?F1a/ RgS1{O7-*SbVu+VeA:8}k#)R1GrVEWQ<A()Fm),k;dr)3u{~=],,#');
define('SECURE_AUTH_KEY',  '[n,]$bfaK@IOh$;$Z_bR-qJCqp_}d%Pg:J#1jNJY:AI06>_;]D#m*s2FbTI+r39)');
define('LOGGED_IN_KEY',    'gY7I&Q$j$vyM46i@D|N`E&uPdg12je|/Y$CUTF}a^i1$Bylx$Mr)X2MJ@|gJ!<JK');
define('NONCE_KEY',        '-=yV-n +Yg&}P@X(zu*X!Lo8Z=n%Y(|,v?+pWoh2ut},L=||aE>Pvf}xR-7_A9=/');
define('AUTH_SALT',        'AgT&_>(0X{z}G~XkWG[PbsK0}CHKJW:nr4o[v8&xtA2-{_}pT{k_`2Kj~xlOqde2');
define('SECURE_AUTH_SALT', 'YCml1sE?O~,;ktYP*8:*YeR2hTi_#j57v{#|2tO+}HYLnpO}AZden:PF2[-4>Dsp');
define('LOGGED_IN_SALT',   '1pXC%!AA]g]<+(N;+IOi@zrAZhX}`rS=k1mw|z&]Nf*}KNwppT*R[ Mcht_=U!M@');
define('NONCE_SALT',       '.ky9RyP6^*x<nokC:|x---r>~)_C0R1;MLN[s-tT((tJFZ]t95E=Jj{V{,=~xbr|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jk_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
